# README #

### What is this repository for? ###

* It gets a specific human readable format of travel routes and returns 
an human readable itinerary, from start to end, including available indications. 

## How do I get set up?

Clone the repo:

    git clone https://cosmoteco@bitbucket.org/cosmoteco/travel-organizer.git

Setup environment:

    docker-compose up -d --build

## Example of usage:

#### By HTML form
Once the project is up access:

    localhost:8080/calculate-itinerary
Add the human readable tickets details in the input textarea, e.g:

```text
Flight TH65 from Chang Mai to Krabi seat 22B gate 7, checkin
Train 1347 from Bangkok to Chang Mai seat 55
Flight FLT11 from Moscow Airport to Bangkok seat 11B gate22
Flight 3CX from Otopeni to Moscow Airport seat22A gate 11, checkin
Ferry from Chang Mai to Phuket seat 77 gate 3, checkin
```
Press submit button and get the itinerary

#### As an API (use a REST client like Postman, Advanced REST client)
Once the project is up use the root:

    POST localhost:8080/itinerary
    
Add json body, e.g:
```json
{
    "input":"Train 1347 from Bangkok to Chang Mai seat 55 \n   Flight FLT11 from Moscow Airport to Bangkok seat 11B gate22 \n Flight 3CX from Otopeni to Moscow Airport seat22A gate 11, checkin\n Flight TH65 from Chang Mai to Krabi seat 22B gate 7, checkin \n Ferry from Chang Mai to Phuket seat 77 gate 3, checkin"
}
```
Submit

Success: 200 Code, json (json encoded string):

    "From Otopeni, board the flight 3CX to Moscow Airport from gate 11, seat 22A. Self checkin luggage at counter.\nFrom Moscow Airport, board the flight FLT11 to Bangkok from gate 22, seat 11B. Luggage will transfer automatically from the last flight.\nBoard the Train 1347 from Bangkok to Chang Mai. Seat number 55.\nFrom Chang Mai, board the ferry  to Phuket from gate 3, seat 77. Self checkin luggage at counter.\n"
Error:
    
    {
        "error": "Transportation mean is missing from a ticket.",
        "code": 400
    }

***Important: For both HTML form and API usages the input needs respect a pattern.***

    Pattern with placeholders:
    {TransportationMean|mandatory} {transportatinMeanNumber|optional} from {startingPoint|mandatory} to {destinatioPoint|mandatory} seat {seatNr|optional} {gate|optional}{,|mandatory}{checkinOption|optinal}
    
    seat, gate and checkin placeholders can be separated by space or comma (one of space or comma is mandatory).
    If both seat and gate are present seat has to be introduced before gate.
    Between gate and checkin a comma is mandatory.
    E.g 
    Flight TH65 from Chang Mai to Krabi seat 22B gate 7, checkin
    
### Mocking    
*** For providing mocks to the other two developers i used StopLight tool which offers the feature of registering ***
*** the application by providing the API url and still inside the tool of writting the mocks in a very user friendly ***
*** interface. ***

## Dependencies
* GIT
* Docker
* json
* twig
* PHP version >= 7.1.3
* cosmoteco/itinerary library (it contains the algorithm to create the itinerary)

### Contribution guidelines ###
* Writing tests
* Code review
