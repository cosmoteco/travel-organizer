<?php
namespace App\Routing;

use App\Traits\RouterUtils;

class Router
{
    use RouterUtils;
    /**
     * @var array
     */
    protected $routes = [];

    protected $variableRoutes = [];

    /**
     * The route group attribute stack.
     *
     * @var array
     */
    protected $groupStack = [];

    const VARIABLE_REGEX = '
    \{
        \s* ([a-zA-Z_][a-zA-Z0-9_-]*) \s*
        (?:
            : \s* ([^{}]*(?:\{(?-1)\}[^{}]*)*)
        )?
    \}';
    const DEFAULT_DISPATCH_REGEX = '[^/]+';

    /**
     * @param $uri
     * @param $action
     *
     * @return $this
     */
    public function get($uri, $action)
    {
        $this->addRoute('GET', $uri, $action);

        return $this;
    }

    /**
     * @param $uri
     * @param $action
     *
     * @return $this
     */
    public function post($uri, $action)
    {
        $this->addRoute('POST', $uri, $action);

        return $this;
    }

    /**
     * Get the raw routes for the application.
     *
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    public function getVariableRoutes()
    {
        return $this->variableRoutes;
    }

    /**
     * Gather the routes from the routes file under a given namespace.
     * @TODO extend for prefix, sufix
     *
     * @param $attributes
     * @param $callback
     */
    public function group($attributes, $callback)
    {
        $this->updateRoutesGrouping($attributes);

        return call_user_func($callback, $this);
    }

    /**
     * @param $method
     * @param $uri
     * @param $action
     */
    public function addRoute($method, $uri, $action)
    {
        $attributes = null;
        if ($this->hasGroupStack()) {
            $attributes = array_merge_recursive(end($this->groupStack), []);
        }

        if (null !== $attributes && is_array($attributes)) {
            $action = $this->applyGroupAttributes($action, $attributes);
        }

        $uri = '/'.trim($uri, '/');

        $routeDatas = $this->parseUri($uri);
        if (!$this->isStaticRoute($routeDatas))
        {
            foreach ($routeDatas as $routeData) {
                $this->addVariableRoute($method, $routeData, $action);
            }

            return;
        }

        $this->routes[$method.$uri] = ['method' => $method, 'uri' => $uri, 'action' => $action];
    }

    /**
     * @param $httpMethod
     * @param $routeData
     * @param $handler
     * @throws \Exception
     */
    private function addVariableRoute($httpMethod, $routeData, $handler)
    {
        list($regex, $variables) = $this->buildRegexForRoute($routeData);

        if (isset($this->variableRoutes[$httpMethod][$regex])) {
            throw new \Exception(sprintf(
                'Two routes matching "%s" for method "%s" are not allowed',
                $regex, $httpMethod
            ));
        }

        $this->variableRoutes[$httpMethod][$regex] = [
            'httpMethod' => $httpMethod,
            'handler' => $handler,
            'variables' => $variables,
            'regex' => $regex,
        ];
    }


    /**
     * @param mixed[]
     * @return bool
     */
    private function isStaticRoute($routeData)
    {
        return count($routeData) === 1 && is_string($routeData[0]);
    }

    /**
     * @param $route
     *
     * @return array
     * @throws \Exception
     */
    protected function parseUri($route)
    {
        $routeWithoutClosingOptionals = rtrim($route, ']');
        $numOptionals = strlen($route) - strlen($routeWithoutClosingOptionals);

        // Split on [ while skipping placeholders
        $segments = preg_split('~' . self::VARIABLE_REGEX . '(*SKIP)(*F) | \[~x', $routeWithoutClosingOptionals);

        if ($numOptionals !== count($segments) - 1) {
            // If there are any ] in the middle of the route, throw a more specific error message
            if (preg_match('~' . self::VARIABLE_REGEX . '(*SKIP)(*F) | \]~x', $routeWithoutClosingOptionals)) {
                throw new \Exception('Optional segments can only occur at the end of a route');
            }
            throw new \Exception("Number of opening '[' and closing ']' does not match");
        }

        $currentRoute = '';
        $routeDatas = [];
        foreach ($segments as $n => $segment) {
            if ($segment === '' && $n !== 0) {
                throw new \Exception('Empty optional part');
            }

            $currentRoute .= $segment;
            $routeDatas[] = $this->parsePlaceholders($currentRoute);
        }

        return $routeDatas;
    }

    /**
     * Parses a route string that does not contain optional segments.
     *
     * @param string
     * @return mixed[]
     */
    private function parsePlaceholders($route)
    {
        if (!preg_match_all(
            '~' . self::VARIABLE_REGEX . '~x', $route, $matches,
            PREG_OFFSET_CAPTURE | PREG_SET_ORDER
        )) {
            return [$route];
        }

        $offset = 0;
        $routeData = [];
        foreach ($matches as $set) {
            if ($set[0][1] > $offset) {
                $routeData[] = substr($route, $offset, $set[0][1] - $offset);
            }
            $routeData[] = [
                $set[1][0],
                isset($set[2]) ? trim($set[2][0]) : self::DEFAULT_DISPATCH_REGEX
            ];
            $offset = $set[0][1] + strlen($set[0][0]);
        }

        if ($offset !== strlen($route)) {
            $routeData[] = substr($route, $offset);
        }

        return $routeData;
    }
}