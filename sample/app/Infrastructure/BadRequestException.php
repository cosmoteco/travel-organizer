<?php

namespace App\Infrastructure;

class BadRequestException extends \Exception
{
    const CODE = 400;
}