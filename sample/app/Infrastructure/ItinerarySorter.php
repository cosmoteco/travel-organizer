<?php

namespace App\Infrastructure;

use Src\Calculator;

class ItinerarySorter implements \App\Model\RouteSorter
{
    private $sorter;

    public function __construct()
    {
        $this->sorter = new Calculator();
    }

    /**
     * @param array $fromToRoutes
     *
     * @return mixed
     * @throws BadRequestException
     */
    public function execute(array $fromToRoutes)
    {
        try {
            // @TODO handle json_decode errors.
            $response = json_decode($this->sorter->sortRoutes($fromToRoutes), 1);
        } catch (\Src\Exception\BadRequestException $ex) {
            throw new BadRequestException(
                $ex->getMessage(),
                400
            );
        } catch (\Exception $ex) {
            throw new \Exception(
                'The itinerary library return internal server error.',
                500
            );
        }

        return $response;
    }
}