<?php

namespace App\Http;

class Request extends BaseRequest implements IRequest
{
    public $requestMethod;

    function __construct()
    {
        $this->bootstrap();
    }

    private function bootstrap()
    {
        foreach($_SERVER as $key => $value)
        {
            $this->{$this->camelCaseify($key)} = $value;
        }
    }

    private function camelCaseify($string)
    {
        $result = strtolower($string);

        preg_match_all('/_[a-z]/', $result, $matches);

        foreach($matches[0] as $match)
        {
            $c = str_replace('_', '', strtoupper($match));
            $result = str_replace($match, $c, $result);
        }

        return $result;
    }

    public function getBody()
    {
        if($this->requestMethod === "GET")
        {
            return;
        }

        if ($this->requestMethod == "POST")
        {
            $body = array();
            foreach($_POST as $key => $value)
            {
                $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }

            return $body;
        }
    }

    /**
     *
     */
    public static function createFromBase(BaseRequest $request)
    {
        if ($request instanceof static) {
            return $request;
        }

        $files = $request->files ? $request->files->all() : null;

        $newRequest = (new static)->duplicate(
            $request->query->all(), $request->request->all(), $request->attributes->all(),
            $request->cookies->all(), $files, $request->server->all()
        );

        $newRequest->headers->replace($request->headers->all());

        $newRequest->content = $request->content;

        $newRequest->request = $newRequest->getInputSource();

        return $newRequest;
    }

    /**
     * {@inheritdoc}
     */
    public function duplicate(array $query = null, array $request = null, array $attributes = null, array $cookies = null, array $files = null, array $server = null)
    {
        return parent::duplicate($query, $request, $attributes, $cookies, $files, $server);
    }
}