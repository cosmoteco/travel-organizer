<?php

namespace App\Http\Controllers;

use App\Http\Request;
use App\Infrastructure\BadRequestException;
use App\Services\CalculateRouteCommand;
use App\Services\CalculateRouteCommandHandler;
use App\Services\View;

class ItineraryController
{
    private $handler;

    public function __construct()
    {
        $this->handler = new CalculateRouteCommandHandler();
    }

    /**
     * @param Request|null $request
     *
     * @return string
     */
    public function calculate(Request $request = null)
    {
        header('Content-type: application/json');

        $input = $request->request->get('input');

        $command = new CalculateRouteCommand(
            $input
        );

        try {
            $response = $this->handler->execute($command);

        } catch (BadRequestException $ex) {
            http_response_code(400);
            return json_encode([
                'error' => $ex->getMessage(),
                'code'  => $ex->getCode()
            ]);
        } catch (\Exception | \Throwable $ex) {
            http_response_code(500);
            return json_encode([
                'error' => $ex->getMessage(),
                'code'  => 500
            ]);
        }

        http_response_code(200);
        return json_encode($response);
    }

    public function renderForm(Request $request = null)
    {
        $input    = '';
        $response = '';
        if ('POST' === $request->getMethod()) {
            $input = $request->request->get('input');
            $command = new CalculateRouteCommand(
                $input
            );

            $errorMessage = '';
            try {
                $response = $this->handler->execute($command);
            } catch (BadRequestException $ex) {
                $errorMessage = $ex->getMessage();
            } catch (\Exception | \Throwable $ex) {
                //Exceptions and Throwable can be be handled separately also.
                $errorMessage = 'Internal Server Error.';
            }
        }

        View::renderTemplate(
            'itinerary.html',
            [
                'errorMessage' => $errorMessage,
                'input'        => $input,
                'response'     => nl2br($response)
            ]
        );
    }
}