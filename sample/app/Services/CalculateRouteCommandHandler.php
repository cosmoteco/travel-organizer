<?php

namespace App\Services;

use App\Infrastructure\BadRequestException;
use App\Infrastructure\ItinerarySorter;
use App\Model\IBoardingTranportation;
use App\Model\ITransportationMean;
use App\Model\TransportationMeanFactory;

class CalculateRouteCommandHandler
{
    private $itinerarySorter;
    public function __construct()
    {
        $this->itinerarySorter = new ItinerarySorter();
    }

    /**
     * @param CalculateRouteCommand $command
     *
     * @return string
     * @throws BadRequestException
     */
    public function execute(CalculateRouteCommand $command)
    {
        try {
            $routes = $this->computeRoutes($command->ticketsDetails());

            $fromToArray = $this->getFromToArray($routes);

            $sortedRoutes = $this->itinerarySorter->execute($fromToArray);

            $translator = new RoutesTranslator();

            return $translator->toHumanReadable($routes, $sortedRoutes);
        } catch(BadRequestException|\Exception $ex) {
            //@TODO the throwable can be manipulated here an passed to controller again.
            //@TODO log the throwable for original details.
            throw $ex;
        } catch (\Throwable $t) {
            //@TODO the throwable can be manipulated here an passed to controller again.
            //@TODO log the throwable for original details.
            throw $t;
        }
    }

    /**
     * Creates an array of from to routes with route details.
     *
     * @param string $data
     * @return array
     *
     * @throws BadRequestException
     */
    protected function computeRoutes(string $data): array
    {
        $ticketsLines = explode(PHP_EOL, $data);
        $routeDetails = [];

        foreach ($ticketsLines as $line) {
            $this->validate($line);

            preg_match_all(
                '/^.*(?=from\s?)|(?<=from).*(?=to\s)|(?<=to).*?(?=,|gate|seat\s?|$)|(?<=seat|gate).*?(?=,|gate|seat|$)|(?<=gate).*?(?=\.|\,|checkin|$)|checkin(?=.|$)/',
                $line,
                $matches
            );

            $matches = $matches[0];

            $transportationMeanFactory = new TransportationMeanFactory();
            $object = $transportationMeanFactory->getType($matches);

            $transportMeanDetails = explode(' ', ltrim(rtrim($matches[0],' '), ' '));

            $object->setName($transportMeanDetails[0]);
            if (isset($transportMeanDetails[1])) {
                $object->setNumber($transportMeanDetails[1]);
            }

            [$from, $to, $seat, $gate] = $this->getCleaned($matches);

            $object->setFrom($from);
            $object->setTo($to);
            $object->setSeat($seat);

            if ($object instanceof IBoardingTranportation) {
                $object->setGate($gate);
                $object->setCheckin(isset($matches[5]));
            }

            $routeDetails[$from.'@'.$to] = $object;
        }

        return $routeDetails;
    }

    /**
     * @param array $routesDetails
     * @return array
     */
    protected function getFromToArray(array $routesDetails): array
    {
        $routes = [];
        /** @var ITransportationMean[] $routesDetails */
        foreach ($routesDetails as $object) {
            $routes[$object->getFrom()] = $object->getTo();
        }

        return $routes;
    }

    /**
     * @param string $line
     *
     * @throws BadRequestException
     */
    protected function validate(string $line)
    {
        if (
            false === strpos($line, ' from ')
            && false === strpos($line, 'from ')
            && false === strpos($line, ' from')
            || false !== strpos($line, 'from to')
        ) {
            throw new BadRequestException('Start information is missing from a ticket.', 400);
        }

        preg_match_all(
            '/^.*(?=from\s?)|(?<=from).*?(?=to|\s)|(?<=to).*?(?=,|gate|seat\s?|$)/',
            $line,
            $matches
        );

        $filteredMatches = array_filter($matches[0]);

        if (false === strpos( $line, ' to ')
            || false === strpos( $line, 'to ')
            || false === strpos( $line,  ' to')
            && (count($filteredMatches) < 3)
        ) {
            throw new BadRequestException('Destination information is missing from a ticket.', 400);
        }

        if (count($filteredMatches) < 3) {
            throw new BadRequestException('Transportation mean is missing from a ticket.', 400);
        }
    }

    /**
     * @param array $matches
     *
     * @return array
     */
    private function getCleaned(array $matches)
    {
        $from =  $matches[1] ? ltrim(rtrim($matches[1],' '), ' ') : null;
        $to = $matches[2] ? str_replace(["\n", "\r"], '', ltrim(rtrim($matches[2],' '), ' ')) : null;
        $seat = $matches[3] ? str_replace(["\n", "\r"], '', ltrim(rtrim($matches[3],' '), ' ')) : null;
        $gate = $matches[4] ? str_replace(["\n", "\r"], '', ltrim(rtrim($matches[4],' '), ' ')) : null;

        return [$from, $to, $seat, $gate];
    }
}