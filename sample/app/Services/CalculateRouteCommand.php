<?php

namespace App\Services;

class CalculateRouteCommand implements Command
{
    /**
     * @var string
     */
    private $ticketsDetails;

    public function __construct(string $ticketsDetails)
    {
        $this->ticketsDetails = $ticketsDetails;
    }

    /**
     * @return string
     */
    public function ticketsDetails(): string
    {
        return $this->ticketsDetails;
    }
}