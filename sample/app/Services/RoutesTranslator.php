<?php

namespace App\Services;

use App\Model\ITransportationMean;

class RoutesTranslator
{
    /**
     * @param ITransportationMean[] $routesObjects
     * @param array $sortedRoutes
     *
     * @return string
     */
    public function toHumanReadable(array $routesObjects, array $sortedRoutes)
    {
        $output = '';

        foreach ($sortedRoutes as $key => $route) {
            $output .= $routesObjects[$key]->indication().PHP_EOL;
        }

        return $output;
    }
}