<?php

namespace App\Model;

interface ITransportationMean
{
    public function setName(string $name);
    public function getName(): string;

    public function setNumber(string $number);
    public function getNumber(): string;

    public function setFrom(string $from);
    public function getFrom(): string;

    public function setTo(string $to);
    public function getTo(): string;

    public function setSeat(?string $seat);
    public function getSeat(): ?string;

    public function indication(): string;
}