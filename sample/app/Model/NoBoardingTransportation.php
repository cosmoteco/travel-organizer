<?php

namespace App\Model;

class NoBoardingTransportation implements ITransportationMean
{
    const NO_SEAT = "No seat assignment";

    private $name;

    private $number = null;

    private $from;

    private $to;

    private $seat = null;

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return NoBoardingTransportation
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumber(): string
    {
        if (empty($this->number)) {
            return '';
        }

        return $this->number;
    }

    /**
     * @param string $number
     * @return NoBoardingTransportation
     */
    public function setNumber(string $number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     * @return NoBoardingTransportation
     */
    public function setFrom(string $from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     * @return NoBoardingTransportation
     */
    public function setTo(string $to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeat(): ?string
    {
        return $this->seat;
    }

    /**
     * @param mixed $seat
     * @return NoBoardingTransportation
     */
    public function setSeat(?string $seat)
    {
        $this->seat = $seat;
        return $this;
    }

    public function getSeatIndication(): string
    {
        $seatIndication = static::NO_SEAT;
        $seat = $this->getSeat();
        if (!empty($seat)) {
            $seatIndication = "Seat number $seat";
        }

        return $seatIndication;
    }

    /**
     * @return string
     */
    public function indication(): string
    {
        return sprintf(
            "Board the %s %s from %s to %s. %s.",
            $this->getName(),
            $this->getNumber(),
            $this->getFrom(),
            $this->getTo(),
            $this->getSeatIndication()
        );
    }
}