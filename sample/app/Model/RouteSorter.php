<?php

namespace App\Model;

interface RouteSorter
{
    public function execute(array $input);
}