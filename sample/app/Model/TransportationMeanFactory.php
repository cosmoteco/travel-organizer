<?php

namespace App\Model;

class TransportationMeanFactory
{
    public function getType(array $input)
    {
        $object = new NoBoardingTransportation();

        $hasGate = $input[4] ?? null;
        if ($hasGate) {
            $object = new BoardingTransportation();
        }

        return $object;
    }
}