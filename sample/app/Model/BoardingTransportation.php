<?php

namespace App\Model;

class BoardingTransportation extends NoBoardingTransportation implements IBoardingTranportation
{
    private $gate;

    private $checkin = false;

    const NO_CHECKIN_ADVICE = 'Luggage will transfer automatically from the last flight';
    const CHECKIN_ADVICE    = 'Self checkin luggage at counter';

    /**
     * @return string
     */
    public function getGate(): ?string
    {
        return $this->gate;
    }

    /**
     * @param mixed $gate
     * @return BoardingTransportation
     */
    public function setGate(?string $gate)
    {
        $this->gate = $gate;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasCheckin(): bool
    {
        return $this->checkin;
    }

    /**
     * @param bool $checkin
     * @return BoardingTransportation
     */
    public function setCheckin(bool $checkin): BoardingTransportation
    {
        $this->checkin = $checkin;
        return $this;
    }

    /**
     * @return string
     */
    public function getCheckinIndication()
    {
        if (!$this->hasCheckin()) {
            return static::NO_CHECKIN_ADVICE;
        }

        return static::CHECKIN_ADVICE;
    }

    /**
     * @return string
     */
    public function indication(): string
    {
        if (!empty($this->getName())) {
            return sprintf(
                "From %s, board the %s %s to %s from gate %s, seat %s. %s.",
                $this->getFrom(),
                strtolower($this->getName()),
                $this->getNumber(),
                $this->getTo(),
                $this->getGate(),
                $this->getSeat(),
                $this->getCheckinIndication()
            );
        }

        return sprintf(
            "From %s, board the %s to %s from gate %s, seat %s. %s.",
            $this->getFrom(),
            $this->getNumber(),
            $this->getTo(),
            $this->getGate(),
            $this->getSeat(),
            $this->getCheckinIndication()
        );
    }
}