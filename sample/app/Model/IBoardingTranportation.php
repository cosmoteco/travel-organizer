<?php

namespace App\Model;

interface IBoardingTranportation extends ITransportationMean
{
    public function setGate(?string $gate);
    public function getGate(): ?string;

    public function setCheckin(bool $checkin);
    public function hasCheckin();
}