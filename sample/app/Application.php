<?php

namespace App;

use App\Http\Request;
use App\Routing\Router;

class Application
{
    use Traits\RoutesRequests;

    /**
     * Router instance
     *
     * @var Router
     */
    public $router;

    /**
     * @var Request
     */
    public $request = null;

    public function __construct()
    {
        $this->router = new Router();
    }

    public function run($request = null)
    {
        $response = $this->dispatch($request);

        echo (string) $response;
    }

    protected function handleVariableRoute($routeData, $uri)
    {
        foreach ($routeData as $data) {
            $regex = '~^(?|'.$data['regex'].'|)$~';

            if (!preg_match($regex, $uri, $matches)) {
                continue;
            }

            $handler = $data['handler'];
            $varNames = $data['variables'];

            $vars = [];
            $i = 0;
            foreach ($varNames as $varName) {
                $vars[$varName] = $matches[++$i];
            }

            return $this->handleFoundRoute($handler);
        }
    }
}