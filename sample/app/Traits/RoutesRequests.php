<?php

namespace App\Traits;

use App\Http\BaseRequest;
use App\Http\Request;

trait RoutesRequests
{
    /**
     * @param $request
     * @return array
     *
     * @throws \Exception
     */
    protected function parseRequest($request = null)
    {
        if (!$request) {
            $this->request = Request::createFromBase(BaseRequest::createFromGlobals());
        }

        return [$this->request->getMethod(), '/'.trim($this->request->getPathInfo(), '/')];
    }

    /**
     * Dispatch the incoming request.
     */
    public function dispatch($request = null)
    {
        [$method, $pathInfo] = $this->parseRequest($request);

        if (isset($this->router->getRoutes()[$method.$pathInfo])) {
            return $this->handleFoundRoute($this->router->getRoutes()[$method.$pathInfo]['action']);
        } else {
            return $this->handleVariableRoute($this->router->getVariableRoutes()[$method], $pathInfo);
        }
    }

    /**
     * @param $routeInfo
     * @return mixed
     */
    protected function handleFoundRoute($routeInfo)
    {
        $uses = $routeInfo;
        [$controllerClass, $action] = explode('@', $uses);

        $instance =  new $controllerClass;

        return call_user_func([$instance, $action], $this->request);
    }
}