<?php

namespace App\Traits;

trait RouterUtils
{
    /**
     * @param $routeData
     * @return array
     * @throws \Exception
     */
    private function buildRegexForRoute($routeData)
    {
        $regex = '';
        $variables = [];
        foreach ($routeData as $part) {
            if (is_string($part)) {
                $regex .= preg_quote($part, '~');
                continue;
            }

            list($varName, $regexPart) = $part;
            if (isset($variables[$varName])) {
                throw new \Exception(sprintf(
                    'Cannot use the same placeholder "%s" twice', $varName
                ));
            }

            if ($this->regexHasCapturingGroups($regexPart)) {
                throw new \Exception(sprintf(
                    'Regex "%s" for parameter "%s" contains a capturing group',
                    $regexPart, $varName
                ));
            }

            $variables[$varName] = $varName;
            $regex .= '(' . $regexPart . ')';
        }

        return [$regex, $variables];
    }

    /**
     * @param string
     * @return bool
     */
    private function regexHasCapturingGroups($regex)
    {
        if (false === strpos($regex, '(')) {
            return false;
        }

        return (bool) preg_match(
            '~
                (?:
                    \(\?\(
                  | \[ [^\]\\\\]* (?: \\\\ . [^\]\\\\]* )* \]
                  | \\\\ .
                ) (*SKIP)(*FAIL) |
                \(
                (?!
                    \? (?! <(?![!=]) | P< | \' )
                  | \*
                )
            ~x',
            $regex
        );
    }

    /**
     * Determine if the router currently has a group stack.
     *
     * @return bool
     */
    public function hasGroupStack()
    {
        return ! empty($this->groupStack);
    }

    /**
     * Use the group attributes namespace, prefix, suffix into the action.
     *
     * @param  string $action
     * @param  array  $attributes The group attributes
     * @return string
     */
    protected function applyGroupAttributes(string $action, array $attributes): string
    {
        $namespace = $attributes['namespace'] ?? null;

        return $this->prependNamespace($action, $namespace);
    }

    /**
     * @param string      $action
     * @param string|null $namespace
     * @return string
     */
    protected function prependNamespace(string $action, string $namespace = null): string
    {
        if (isset($namespace, $action)) {
            $class          = $action;
            $action = $namespace !== null && strpos($class, '\\') !== 0
                ? $namespace.'\\'.$class : $class;
        }

        return $action;
    }

    /**
     * Update the group stack with the given attributes.
     *
     * @param  array  $attributes
     * @return void
     */
    protected function updateRoutesGrouping(array $attributes)
    {
        if (!empty($this->groupStack)) {
            $attributes = array_merge_recursive(end($this->groupStack), $attributes);
        }

        $this->groupStack[] = $attributes;
    }
}