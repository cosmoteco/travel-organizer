<?php
require_once __DIR__.'/../vendor/autoload.php';

$app = new \App\Application();

$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__.'/../routes/web.php';
});

return $app;