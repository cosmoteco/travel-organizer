<?php declare(strict_types=1);

class CalculateRootCommandHandlerTest extends \PHPUnit\Framework\TestCase
{
    public function testThatExecuteMethodReturnsCorrectOutput()
    {
        $input = "Flight TH65 from Chang Mai to Krabi seat 22B gate 7, checkin
Train 1347 from Bangkok to Chang Mai seat 55
Flight FLT11 from Moscow Airport to Bangkok seat 11B gate22
Flight 3CX from Otopeni to Moscow Airport seat22A gate 11, checkin
Ferry from Chang Mai to Phuket seat 77 gate 3, checkin";
        $command = new \App\Services\CalculateRouteCommand($input);

        $handler = new \App\Services\CalculateRouteCommandHandler($command);
        $result = $handler->execute($command);

        $output = 'From Otopeni, board the flight 3CX to Moscow Airport from gate 11, seat 22A. Self checkin luggage at counter.
From Moscow Airport, board the flight FLT11 to Bangkok from gate 22, seat 11B. Luggage will transfer automatically from the last flight.
Board the Train 1347 from Bangkok to Chang Mai. Seat number 55.
From Chang Mai, board the ferry  to Phuket from gate 3, seat 77. Self checkin luggage at counter.
';
        $this->assertEquals(nl2br($output), nl2br($result));
    }

    public function testThatExecuteMethodThrowsMissingTransportationMeanException()
    {
        $this->expectExceptionMessage('Transportation mean is missing from a ticket.');

        $input = "from Test to test2 seat 3";
        $command = new \App\Services\CalculateRouteCommand($input);

        $handler = new \App\Services\CalculateRouteCommandHandler($command);
        $handler->execute($command);
    }

    public function testThatExecuteMethodThrowsMissingStartingPointException()
    {
        $this->expectExceptionMessage('Start information is missing from a ticket.');

        $input = "Train 1347 from to";
        $command = new \App\Services\CalculateRouteCommand($input);

        $handler = new \App\Services\CalculateRouteCommandHandler($command);
        $handler->execute($command);
    }

    public function testThatExecuteMethodThrowsMissingDestinationPointException()
    {
        $this->expectExceptionMessage('Destination information is missing from a ticket.');

        $input = "Flight 3CX from start to";
        $command = new \App\Services\CalculateRouteCommand($input);

        $handler = new \App\Services\CalculateRouteCommandHandler($command);
        $handler->execute($command);
    }

    public function testThatLibraryThrowsExceptionThatCannotMakeTheItinerary()
    {
        $this->expectExceptionMessage("Can't determine the itinerary by Phuket starting point. Check that one ticket departure matches with another's destination!");

        // changed departure from Bangkok to Bankok
        $input = "Flight TH65 from Chang Mai to Krabi seat 22B gate 7, checkin
Train 1347 from Bankok to Chang Mai seat 55
Flight FLT11 from Moscow Airport to Bangkok seat 11B gate22
Flight 3CX from Otopeni to Moscow Airport seat22A gate 11, checkin
Ferry from Chang Mai to Phuket seat 77 gate 3, checkin";
        $command = new \App\Services\CalculateRouteCommand($input);

        $handler = new \App\Services\CalculateRouteCommandHandler($command);
        $handler->execute($command);
    }
}