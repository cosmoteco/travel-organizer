<?php
$router->get('test','ItineraryController@test');

$router->get('itinerary/offset/{offset}/limit/{limit}', 'ItineraryController@calculateGet');

$router->post('itinerary', 'ItineraryController@calculate');

$router->get('calculate-itinerary', 'ItineraryController@renderForm');
$router->post('calculate-itinerary', 'ItineraryController@renderForm');

